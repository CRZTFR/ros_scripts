import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
import math

class move_robot:
    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.group = moveit_commander.MoveGroupCommander("manipulator")
        self.display_trajectory_publisher = rospy.Publisher('move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory, 1)
        print('Connected to robot')

    def move_to_start(self):
        print(self.robot.get_current_state())
        # Create the start position
        start_pos = geometry_msgs.msg.Pose()
        # start_pos.orientation.x = 0.015352
        # start_pos.orientation.y = -0.71048
        # start_pos.orientation.z = 0.026547
        # start_pos.orientation.w = 0.70304
        # start_pos.position.x = -0.82336
        # start_pos.position.y = 0.15795
        # start_pos.position.z = 0.48447
        start_pos.orientation.x = 0.50798
        start_pos.orientation.y = -0.48362
        start_pos.orientation.z = 0.52116
        start_pos.orientation.w = 0.48627
        start_pos.position.x = -0.72357
        start_pos.position.y = 0.16388
        start_pos.position.z = 0.48548
        self.start_pos = start_pos
        plan = self.group.plan()
        rospy.sleep(5)
        self.group.go(wait=True)
        self.group.clear_pose_targets()

    def joints_to_start(self):
        joints = [0.7997236251831055, -2.393949171105856, -0.7393617630004883, 0.009716196651122999, 1.5698556900024414, 1.065929889678955]
        #joints = [0,0,0,0,0,0]
        self.group.set_joint_value_target(joints)
        plan = self.group.plan()
        print('Moving to start')
        self.group.go(wait=True)
        rospy.sleep(2)

    def scoot_left(self):
        #joints = [-0.03676349321474248, -2.525692125360006, -0.7375497817993164, 0.11136834203686519, 1.5398998260498047, 1.1548051834106445]
        joints = [0,0,0,0,0,0]
        self.group.set_joint_value_target(joints)
        plan = self.group.plan()
        print('Scooting Left')
        self.group.go(wait=True)

    def move_in_routine(self,routine):
            for joints in routine:
                if rospy.is_shutdown():
                    break
                else:
                    try:
                        self.group.set_joint_value_target(joints)
                        plan = self.group.plan()
                        print('Moving ...')
                        self.group.go(wait=True)
                        print('Done')
                    except:
                        print('Joint state was invalid')
                        self.group.clear_pose_targets()


    def get_ind(self,joint_name):
        try:
            if joint_name == 'shoulder_pan_joint':
                return 0
            elif joint_name == 'shoulder_lift_joint':
                return 1
            elif joint_name == 'elbow_joint':
                return 2
            elif joint_name == 'wrist_1_joint':
                return 3
            elif joint_name == 'wrist_2_joint':
                return 4
            elif joint_name == 'wrist_3_joint':
                return 5
        except:
            print(joint_name,' is not a valid joint')

    def plan_movement(self,joint_name,angle):
         ind = self.get_ind(joint_name)
         joints = self.group.get_current_joint_values()
         joints[ind] = joints[ind] + math.radians(angle)
         self.group.set_joint_value_target(joints)
         plan = self.group.plan()
         print('moving ',joint_name,' ',angle,' degrees')

    def go(self):
        self.group.go(wait=True)

if __name__=='__main__':
    rospy.init_node('robot_movements',anonymous=True)
    #rospy.sleep(10)
    print('moving robot')
    while not rospy.is_shutdown():
        try:
            ur5 = move_robot()
            ur5.joints_to_start()
            joints = []
            joints.append([1.965181827545166, -1.7335006199278773, -0.9982404708862305, -0.9870971006206055, 0.21321630477905273, 1.065810203552246]) # Top Right
            joints.append([1.1686625480651855, -2.530095716516012, -0.8336687088012695, -0.1781185430339356, 0.9004530906677246, 2.749960422515869]) # Bottom Right
            joints.append([0.44890642166137695, -1.1601055425456543, -0.8662424087524414, -1.3422907155803223, 1.4855527877807617, -0.11773139635194951]) # Top Left
            joints.append([0.3777642250061035, -2.57608761409902, -0.8309478759765625, 0.3239824014851074, 1.5499343872070312, 1.0183892250061035]) # Bottom left
            ur5.move_in_routine(joints)

            #ur5.scoot_left()
            #ur5.plan_movement('wrist_3_joint',90)
            #ur5.plan_movement('wrist_2_joint',45)
            #ur5.go()
        except rospy.ROSInterruptException:
            pass
