import roslib
import rospy
import math
import tf2_ros
#from tf import TransformListener
import tf2_msgs.msg
import geometry_msgs.msg
import numpy as np
import cv2
import tf_conversions
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import sys
from sensor_msgs.msg import JointState
import matplotlib.pyplot as plt
"""
Transforms the current tool position to a projector coordinate
ros axis colors:
red - x - towards robot
green - y - left
blue - z - up
"""
img = np.zeros((512,512,3), np.uint8)


class find_transform:
    def __init__(self):
        #self.pub = rospy.Publisher('robots_in_disguise', PoseStamped, queue_size=1)
        self.img_pub = rospy.Publisher("image",Image, queue_size = 1)
        self.bridge = CvBridge()
        self.tfBuffer = tf2_ros.Buffer()
        self.tf = tf2_ros.TransformListener(self.tfBuffer)
        #self.tf_pub = rospy.Publisher("/tf_static", tf2_msgs.msg.TFMessage, queue_size = 1)
        self.tf_broadcaster = tf2_ros.StaticTransformBroadcaster()
        self.joint_sub = rospy.Subscriber("joint_states", JointState, self.joint_callback)
        #self.tf_sub = rospy.Subscriber("tf", tf2_msgs.msg.TFMessage, self.tfCallback)
        # Resolution of the projector
        self.hRes = 1920
        self.vRes = 1080
        self.aspectRatio = float(self.hRes)/float(self.vRes)
        # number of m of screen width to distance from screen
        self.throw_ratio = 1.46

        # Size of the projecting surface
        self.shapeHeight = 0.7

        self.img = np.zeros((self.vRes,self.hRes,3), np.uint8)
        self.dog = cv2.imread('dog.jpg')
        x_offset=y_offset=0
        self.img[y_offset:y_offset+self.dog.shape[0], x_offset:x_offset+self.dog.shape[1]] = self.dog
        dogAspectRatio = float(self.dog.shape[1])/float(self.dog.shape[0])
        self.shapeWidth = self.shapeHeight*dogAspectRatio
        self.shapeWidth = 0.5
        self.hRes = 1280
        self.vRes = 800
        # create the window to display the image
        cv2.namedWindow('image',cv2.WINDOW_NORMAL)
        cv2.waitKey(1)
        cv2.imshow('image',self.img)
        cv2.waitKey(100)
        tf = None
        self.revised = []
        self.wrist_3 = []
        self.reported = []
        self.count = [0]
        # Initialize the previous transform
        self.create_tf_to_realSense()
        print('looking for transform...')
        while tf is None:
            try:
                tf= self.tfBuffer.lookup_transform('camera_link', 'tool0', rospy.Time(0), rospy.Duration(1))
            except:
                if rospy.is_shutdown():
                    break
                else:
                    print('could not find transform. Retrying')
                    continue
        print('Found transform')
        euler = tf_conversions.transformations.euler_from_quaternion((tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z, tf.transform.rotation.w))
        self.prev_tf_angle = euler[1]
        self.prev_revised_angle = euler[1]
        self.move_flag = 0
        self.plot_flag = 1
        #self.move = demo_movements.move_robot()
    def joint_callback(self, msg):
        self.joint_pos = msg.position
        self.joint_vel = msg.velocity

    def tfCallback(self, tf):
        # Called when a tf message is published
        # Update the transform so that robot_base is connected to the camera
        self.create_tf_to_realSense()

    def create_tf_to_realSense(self):
        # Create a transform from the canvas to the realsense base
        # assuming that the centre of the canvas is at the end of /wrist_3_link
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "/base_link"
        t.child_frame_id = "/camera_link"
        t.transform.translation.x = -3.6
        t.transform.translation.y = -0.25
        t.transform.translation.z = 0.50
        q = tf_conversions.transformations.quaternion_from_euler(0, 0, 0)
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]
        #tfm = tf2_msgs.msg.TFMessage([t])
        #self.tf_pub.publish(tfm)
        self.tf_broadcaster.sendTransform(t)
        #print('created transform from robot to realsense')


    def tf_to_projector(self):
        # Get the distance from the projector to the realsense
        #self.create_tf_to_realSense()
        #t = self.tf.getLatestCommonTime("wrist_3_link", "camera_link")
        tf= self.tfBuffer.lookup_transform('camera_link', 'tool0', rospy.Time(0), rospy.Duration(0.1))
        euler = tf_conversions.transformations.euler_from_quaternion((tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z, tf.transform.rotation.w))
        # #print(pose)
        # # Get joint states
        joints = self.joint_pos
        tf_angle = euler[1]
        angle_dif = euler[1] - self.prev_tf_angle

        wrist_3_angle = joints[5]
        wrist_3_vel= self.joint_vel[5]
        # wrist_2_vel = self.joint_vel[4]
        # if abs(wrist_3_vel) < 0.0001:
        #     print('zero vel')
        #     revised_angle = self.prev_revised_angle
        if tf_angle < self.prev_tf_angle and wrist_3_vel > 0:
            print('angle has lost the plot')
            revised_angle = self.prev_revised_angle - angle_dif
            if self.move_flag > 1:
                self.move_flag = 0
            else:
                self.move_flag -= 1
            if self.move_flag < -3:
                self.plot_flag = 2
                self.move_flag = 0
        elif tf_angle > self.prev_tf_angle and wrist_3_vel < 0:
            print('angle has lost the plot but backwards this time')
            revised_angle = self.prev_revised_angle - angle_dif
            if self.move_flag > 1:
                self.move_flag = 0
            else:
                self.move_flag -= 1
            print('Case 2')
            if self.move_flag < -3:
                self.plot_flag = 2
                self.move_flag = 0
        else:
            self.move_flag += 1
            print('Case 1')
            if self.move_flag > 10:
                revised_angle = self.prev_tf_angle + angle_dif
                self.move_flag = 0
                self.plot_flag = 1
            else:
                revised_angle = self.prev_revised_angle + angle_dif

        self.prev_revised_angle = revised_angle
        self.prev_tf_angle = tf_angle
        # if revised_angle > math.radians(90):
        #     print('> 90')
        #     revised_angle = -revised_angle
        # elif revised_angle < 0:
        #     print('< 0')
        #     print('Case 1')
            #self.plot_flag = 1
            #revised_angle = revised_angle - math.radians(180)
        # else:
        #     if euler[1] < 0:
        #             revised_angle = euler[1] + math.radians(180)
        #     else:
        #         revised_angle = -euler[1]

        self.revised.append(revised_angle)
        self.wrist_3.append(wrist_3_angle)
        self.reported.append(tf_angle)
        self.count.append(self.count[-1] + 1)
        if self.count[-1] + 1 > 1000:
            del self.count[-1]
            plt.plot(self.count,self.reported)
            plt.plot(self.count,self.wrist_3)
            plt.plot(self.count,self.revised)
            plt.legend(['reported','wrist3','revised'])
            plt.title('Errors in reported transform from UR5e')
            plt.xlabel('Timesteps')
            plt.ylabel('Angle (rad)')
            plt.show()
        q = tf_conversions.transformations.quaternion_from_euler(euler[0],revised_angle,euler[2])
        tf.transform.rotation.x = q[0]
        tf.transform.rotation.y = q[1]
        tf.transform.rotation.z = q[2]
        tf.transform.rotation.w = q[3]
        print(math.degrees(revised_angle),math.degrees(euler[1]), math.degrees(wrist_3_angle), wrist_3_vel, math.degrees(angle_dif))
        print(self.plot_flag)
        return tf


    # Takes x,y,z coordinates of a point and returns projector coordinates
    def world_to_projector(self,x,y,dist):
        # Total area that the projector covers
        projWidth = self.throw_ratio*dist        # Size in metres
        projHeight = projWidth/self.aspectRatio
        # Number of pixels per metre
        vppm = projHeight/self.vRes
        hppm = projWidth/self.hRes

        # We may need to apply an offset for projector tilt etc
        # Assume the x and y distances are from the centre of the projector frame
        y_pixel = int(((projHeight/2-y)/vppm))
        x_pixel = int(((projWidth/2-x)/hppm))
        return [x_pixel,y_pixel]

    def rotate(self,point,origin,angle):
        ox,oy = origin
        px,py = point
        #angle = math.radians(180)
        #if angle < 0:
        if self.plot_flag is 1:
            #ngle = math.radians(90)-angle
            qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
            qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        else:
            qx = ox - math.cos(angle) * (px - ox) + math.sin(angle) * (py - oy)
            qy = oy - math.sin(angle) * (px - ox) - math.cos(angle) * (py - oy)

        return [qx, qy]



    def create_square(self,tf):
        # convert pixels to the projector frame
        # Distance from the projector to the wrist_3_link
        screen_dist = tf.transform.translation.x-0.8
        # Distance from the centre of the projecting surface to the wrist_3_link
        y_world = tf.transform.translation.y+0.1
        z_world = tf.transform.translation.z-0
        euler = tf_conversions.transformations.euler_from_quaternion((tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z, tf.transform.rotation.w))
        #print('x: ', tf.transform.translation.x, 'y: ', tf.transform.translation.y, 'z: ', tf.transform.translation.z)
        x_rot = -euler[1]
        y_rot = euler[0]
        z_rot = euler[2]

        #print(math.degrees(x_rot),math.degrees(y_rot),math.degrees(z_rot))
        # Find the distances of each corner from the projector
        # Invert the distances so that the image appears flat all the time
        lDist = +self.shapeWidth/2*math.sin(z_rot)
        rDist = -self.shapeWidth/2*math.sin(z_rot)
        tDist = -self.shapeHeight/2*math.sin(y_rot)
        bDist = +self.shapeHeight/2*math.sin(y_rot)
        tlDist = screen_dist + tDist + lDist
        trDist = screen_dist + tDist + rDist
        blDist = screen_dist + bDist + lDist
        brDist = screen_dist + bDist + rDist

        # Get the world x,y coords of each point
        tl = [y_world-self.shapeWidth/2,z_world+self.shapeHeight/2]
        tr = [y_world+self.shapeWidth/2,z_world+self.shapeHeight/2]
        bl = [y_world-self.shapeWidth/2,z_world-self.shapeHeight/2]
        br = [y_world+self.shapeWidth/2,z_world-self.shapeHeight/2]

        # Rotate the points around the x axis
        centre = [y_world,z_world]
        tl = self.rotate(tl,centre,x_rot)
        tr = self.rotate(tr,centre,x_rot)
        bl = self.rotate(bl,centre,x_rot)
        br = self.rotate(br,centre,x_rot)

        # Convert the coordinates into pixel coordinates
        # pts: tl,tr,br,bl
        pts = np.array([self.world_to_projector(tl[0],tl[1],tlDist),
                            self.world_to_projector(tr[0],tr[1],trDist),
                            self.world_to_projector(br[0],br[1],brDist),
                            self.world_to_projector(bl[0],bl[1],blDist)],
                            np.int32)
        return pts

    def draw_square(self,pts):
        # Create the image
        img = np.zeros((self.vRes,self.hRes,3), np.uint8)
        # Draw the shape and diagonals
        cv2.polylines(img,[pts.reshape(-1,1,2)],True,(0,255,0))
        cv2.line(img,(pts[0][0],pts[0][1]), (pts[2][0],pts[2][1]),(0,255,0))
        cv2.line(img,(pts[1][0],pts[1][1]), (pts[3][0],pts[3][1]),(0,255,0))

        #Display the image
        cv2.imshow('image',img)
        cv2.waitKey(1)

    def display_image(self,pts):
        img = np.zeros((self.vRes,self.hRes,3), np.uint8)
        rows,cols,ch = self.dog.shape
        # Find the max width and height of the distorted image
        # Bottom right - bottom left
        widthA = np.sqrt(((pts[2][0] - pts[3][0])**2) + ((pts[2][1] - pts[3][1])**2))
        # top right - top left
        widthB = np.sqrt(((pts[1][0] - pts[0][0])**2) + ((pts[1][1] - pts[0][1])**2))
        maxWidth = max(int(widthA), int(widthB))
        # Top right - bottom right
        heightA = np.sqrt(((pts[1][0] - pts[2][0])**2) + ((pts[1][1] - pts[2][1])**2))
        # Top left - bottom left
        heightB = np.sqrt(((pts[0][0] - pts[3][0])**2) + ((pts[0][1] - pts[3][1])**2))
        maxHeight = max(int(heightA), int(heightB))
        pts2 = np.float32([[0,0],[0,cols],[rows,cols],[rows,0]])
        pts = np.float32(pts)
        try:
            M = cv2.getPerspectiveTransform(pts2,pts)
            dst = cv2.warpPerspective(self.img,M,(self.hRes,self.vRes))
            cv2.imshow('image',dst)
            cv2.waitKey(1)
        except Exception as e:
            print(e)

def main(args):
    # Transformers
    rospy.init_node('Project_image', anonymous=True)
    ft = find_transform()
    print('Autobots assemble')
    rate = rospy.Rate(1000)
    while not rospy.is_shutdown():
        #ft.create_tf_to_realSense()
        transform = ft.tf_to_projector()
        if transform is not 1:
            #ft.display_square(transform)
            pts = ft.create_square(transform)
            #ft.display_image(pts)
	    ft.draw_square(pts)

        rate.sleep()
    #rospy.spin()

if __name__ == '__main__':
    main(sys.argv)
