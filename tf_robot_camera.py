"""
Publish a transform from camera to robot
"""
import rospy
import tf2_ros
import geometry_msgs.msg
import tf_conversions
def create_tf_to_realSense(tf_broadcaster):
      # Create a transform from the canvas to the realsense base
      # assuming that the centre of the canvas is at the end of /wrist_3_link
      t = geometry_msgs.msg.TransformStamped()
      t.header.stamp = rospy.Time.now()
      t.header.frame_id = "robot_base"
      t.child_frame_id = "camera_link"
      t.transform.translation.x = 0.5
      t.transform.translation.y = 2
      t.transform.translation.z = 1
      q = tf_conversions.transformations.quaternion_from_euler(0, 0, 180)
      t.transform.rotation.x = q[0]
      t.transform.rotation.y = q[1]
      t.transform.rotation.z = q[2]
      t.transform.rotation.w = q[3]
      #tfm = tf2_msgs.msg.TFMessage([t])
      #self.tf_pub.publish(tfm)
      tf_broadcaster.sendTransform(t)
      print('created transform from robot to realsense')

rospy.init_node('robots_in_disguise', anonymous=True)
tf_broadcaster = tf2_ros.StaticTransformBroadcaster()
rate = rospy.Rate(10)
while not rospy.is_shutdown():
    create_tf_to_realSense(tf_broadcaster)
    rate.sleep()
