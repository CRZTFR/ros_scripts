import rospy
from sensor_msgs.msg import Image
import numpy as np
import sys
from cv_bridge import CvBridge, CvBridgeError
import cv2 as cv

class find_rectangle:
    def __init__(self):
        self.img_pub = rospy.Publisher("image",Image, queue_size=1)
        self.bridge = CvBridge()
        self.img_sub = rospy.Subscriber("/camera/depth/image_rect_raw", Image, self.callback)


    def callback(self,data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "16UC1")
            # convert to 8 bit image
            cv_image = (cv_image/256).astype('uint8')
            image = self.remove_background(cv_image, 11)
            squares = self.findSquare(image)
            cv.drawContours(image, squares, -1, (0,255,0),1)
            try:
                image_message = self.bridge.cv2_to_imgmsg(image, "8UC1")
                image_message.encoding = '8UC1'
                self.img_pub.publish(image_message)
                print('publishing')
            except CvBridgeError as e:
                print(e)
        except CvBridgeError as e:
            print(e)


    def remove_background(self,cv_image, threshold):
        (rows,cols) = cv_image.shape
        max = 0
        for i in range(rows):
            for j in range(cols):
                if cv_image[i,j] > max:
                    max = cv_image[i,j]
                # Threshold the image
                # Higher numbers are background -> a higher threshold removes less background
                if cv_image[i,j] > threshold:
                    cv_image[i,j] = 0
                else:
                    cv_image[i,j] = 255
        print(max)
        return cv_image

    def findSquare(self,image):
        # Reduce noise with a blur
        image = np.uint8(image)
        image = cv.GaussianBlur(image,(5,5),0)
        # Find edges
        image = cv.Canny(image, 0, 50, apertureSize = 5)
        image = cv.dilate(image, None)
        bin, contours, _heirachy = cv.findContours(image, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
        # Loop through contours and find squares
        squares = []
        for cnt in contours:
            cnt_len = cv.arcLength(cnt, True)
            cnt = cnt.reshape(-1,2)
            max_cos = np.max([self.angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
            if max_cos < 0.1:
                squares.append(cnt)
                print('found a square')
        return squares

    def angle_cos(self,p0, p1, p2):
        d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
        return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

def main(args):
    fr = find_rectangle()
    rospy.init_node('listener', anonymous=True)
    rospy.spin()

if __name__ == '__main__':
    main(sys.argv)
