import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
import math
import tf
import tf2_ros
import numpy as np
import quaternion as quat
import scipy.optimize



init_joints = {
    1: [1.965181827545166, -1.7335006199278773, -0.9982404708862305, -0.9870971006206055, 0.21321630477905273, 1.065810203552246],
    2: [1.1686625480651855, -2.530095716516012, -0.8336687088012695, -0.1781185430339356, 0.9004530906677246, 2.749960422515869],
    3: [0.44890642166137695, -1.1601055425456543, -0.8662424087524414, -1.3422907155803223, 1.4855527877807617, -0.11773139635194951],
    4: [0.3777642250061035, -2.57608761409902, -0.8309478759765625, 0.3239824014851074, 1.5499343872070312, 1.0183892250061035]
}

trans_cam_array = []
trans_rob_array = []
rot_cam_array = []
rot_rob_array = []


class move_robot:


    start = [0.7997236251831055, -2.393949171105856, -0.7393617630004883, 0.009716196651122999, 1.5698556900024414, 1.065929889678955]


    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.group = moveit_commander.MoveGroupCommander("manipulator")
        self.display_trajectory_publisher = rospy.Publisher('move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory, 1)
        print('Connected to robot')
        rospy.sleep(2)

    def move_to_start(self):
        print(self.robot.get_current_state())
        # Create the start position
        start_pos = geometry_msgs.msg.Pose()
        # start_pos.orientation.x = 0.015352
        # start_pos.orientation.y = -0.71048
        # start_pos.orientation.z = 0.026547
        # start_pos.orientation.w = 0.70304
        # start_pos.position.x = -0.82336
        # start_pos.position.y = 0.15795
        # start_pos.position.z = 0.48447
        start_pos.orientation.x = 0.50798
        start_pos.orientation.y = -0.48362
        start_pos.orientation.z = 0.52116
        start_pos.orientation.w = 0.48627
        start_pos.position.x = -0.72357
        start_pos.position.y = 0.16388
        start_pos.position.z = 0.48548
        self.start_pos = start_pos
        plan = self.group.plan()
        rospy.sleep(5)
        self.group.go(wait=True)
        self.group.clear_pose_targets()

    def move_joints(self,joints):
        self.group.set_joint_value_target(joints)
        plan = self.group.plan()
        self.group.go(wait=True)

    def move_in_routine(self,routine):
        for joints in routine:
            if rospy.is_shutdown():
                break
            else:
                try:
                    self.group.set_joint_value_target(joints)
                    plan = self.group.plan()
                    print('Moving ...')
                    self.group.go(wait=True)
                    print('Done')
                except:
                    print('Joint state was invalid')
                    self.group.clear_pose_targets()

    def get_ind(self,joint_name):
        try:
            if joint_name == 'shoulder_pan_joint':
                return 0
            elif joint_name == 'shoulder_lift_joint':
                return 1
            elif joint_name == 'elbow_joint':
                return 2
            elif joint_name == 'wrist_1_joint':
                return 3
            elif joint_name == 'wrist_2_joint':
                return 4
            elif joint_name == 'wrist_3_joint':
                return 5
        except:
            print(joint_name,' is not a valid joint')

    def plan_movement(self,joint_name,angle):
         ind = self.get_ind(joint_name)
         joints = self.group.get_current_joint_values()
         joints[ind] = joints[ind] + math.radians(angle)
         self.group.set_joint_value_target(joints)
         plan = self.group.plan()
         print('moving ',joint_name,' ',angle,' degrees')

    def go(self):
        self.group.go(wait=True)

def tf_init(robot, listener, broadcaster):

    for key,joints in init_joints.items():
        print('Moving robot to init position %d', key)
        try:
            robot.move_joints(joints)
        except:
            continue
        
        for i in range(0,5):     
            while True:
                try:
                    (trans_camera,rot_camera) = listener.lookupTransform('/t0', '/camera_link',rospy.Time(0))
                except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                    continue
            
                try:
                    (trans_robot,rot_robot) = listener.lookupTransform('/base_link','/tool0_controller', rospy.Time(0))
                except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    continue

                trans_cam_array.append(trans_camera)
                trans_rob_array.append(trans_robot)
                rot_cam_array.append(rot_camera)
                rot_rob_array.append(rot_robot)
                print('good')

                rospy.sleep(0.5)
                break    
        
            print('Initalisation data gathered now processing')        
            
            print('Calculating initial guess for')

            # create homogeneous rotation and translation matrices for inital point
            trans_cam = tf.transformations.translation_matrix(trans_cam_array[0])
            rot_cam   = tf.transformations.quaternion_matrix(rot_cam_array[0])
            mat_cam = tf.transformations.concatenate_matrices(trans_cam,rot_cam)


            trans_rob = tf.transformations.translation_matrix(trans_rob_array[0])
            rot_rob   = tf.transformations.quaternion_matrix(rot_rob_array[0])
            mat_rob = tf.transformations.concatenate_matrices(trans_rob,rot_rob)

            # calculate transform from base_link to camera_link for inital reading
            mat_rob2cam = tf.transformations.concatenate_matrices(mat_rob,mat_cam)

            mat3_cam2rob = tf.transformations.inverse_matrix(mat_rob2cam)

            #decompose into euler and translation
            _, _, angles, translate, _ = tf.transformations.decompose_matrix(mat3_cam2rob)

            #make single vector
            t_cam2rob =  np.concatenate((translate, angles), axis=0)

            #transform_average()

            topt_cam2rob = scipy.optimize.fmin(transform_opt, t_cam2rob)

            trans_opt = tuple(topt_cam2rob[0:3])
            angles_opt = tf.transformations.quaternion_from_euler(topt_cam2rob[3], topt_cam2rob[4], topt_cam2rob[5], axes='sxyz')

            broadcaster.sendTransform(trans_opt,angles_opt,rospy.Time.now(),'/base_link','/camera_link')

            return topt_cam2rob



def transform_opt(transform):
    err = 0
    Ident = np.identity(4)
    angles = transform[3:6]
    trans = transform[0:3]
    mat_cam2rob = tf.transformations.compose_matrix(None, None, angles, trans, None)

    for i in range(0,len(trans_cam_array)):
        trans_cam = tf.transformations.translation_matrix(trans_cam_array[i])
        rot_cam   = tf.transformations.quaternion_matrix(rot_cam_array[i])
        mat_cam = tf.transformations.concatenate_matrices(trans_cam,rot_cam)

        trans_rob = tf.transformations.translation_matrix(trans_rob_array[i])
        rot_rob   = tf.transformations.quaternion_matrix(rot_rob_array[i])
        mat_rob = tf.transformations.concatenate_matrices(trans_rob,rot_rob)

        mat_rob2cam = tf.transformations.concatenate_matrices(mat_rob,mat_cam)

        home = tf.transformations.concatenate_matrices(mat_rob2cam,mat_cam2rob)
        diff = home - Ident
        err += np.linalg.norm(diff)
    return err

def transform_average():
    angles_av = [0,0,0]
    trans_av = [0,0,0]
    length = len(trans_cam_array)
    print(length)
    for i in range(0,length):
        trans1_cam = tf.transformations.translation_matrix(trans_cam_array[i])
        rot1_cam   = tf.transformations.quaternion_matrix(rot_cam_array[i])

        trans2_rob = tf.transformations.translation_matrix(trans_rob_array[i])
        rot2_rob   = tf.transformations.quaternion_matrix(rot_rob_array[i])

        mat3 = transform_add(trans1_cam,rot1_cam,trans2_rob,rot2_rob)

        mat3_inverse = tf.transformations.inverse_matrix(mat3)
        _, _, angles, translate, _ = tf.transformations.decompose_matrix(mat3_inverse)
        angles_av = [x + y for x, y in zip(angles_av, angles)]
        trans_av = [x + y for x, y in zip(trans_av, translate)]
    angles_av = [x/length for x in angles_av]  
    trans_av = [x/length for x in trans_av]  
    print(trans_av,angles_av)


if __name__=='__main__':  
    rospy.init_node('turtle_tf_listener')
    tf_listener = tf.TransformListener()
    tf_broadcaster= tf.TransformBroadcaster()
    broadcaster = tf2_ros.StaticTransformBroadcaster()

    rospy.sleep(2)
    running = True
    while not rospy.is_shutdown():
        while running:
            try:
                ur5 = move_robot()
                running = False
            except rospy.ROSInterruptException:
                pass

        #initialise robot
        topt_cam2rob  = tf_init(ur5, tf_listener, tf_broadcaster)


        static_transformStamped = geometry_msgs.msg.TransformStamped()
  
        static_transformStamped.header.stamp = rospy.Time.now()
        static_transformStamped.header.frame_id = "base_link"
        static_transformStamped.child_frame_id = "camera_link"
  
        static_transformStamped.transform.translation = 
        static_transformStamped.transform.rotation = 
 
        broadcaster.sendTransform(static_transformStamped)
        rospy.spin()
    
        #move in pattern
        print('Main Loop')
        running = True
        while running:
    
            try:
                ur5.move_joints(ur5.start)
                rospy.sleep(2)
                joints = []
                joints.append([1.965181827545166, -1.7335006199278773, -0.9982404708862305, -0.9870971006206055, 0.21321630477905273, 1.065810203552246]) # Top Right
                joints.append([1.1686625480651855, -2.530095716516012, -0.8336687088012695, -0.1781185430339356, 0.9004530906677246, 2.749960422515869]) # Bottom Right
                joints.append([0.44890642166137695, -1.1601055425456543, -0.8662424087524414, -1.3422907155803223, 1.4855527877807617, -0.11773139635194951]) # Top Left
                joints.append([0.3777642250061035, -2.57608761409902, -0.8309478759765625, 0.3239824014851074, 1.5499343872070312, 1.0183892250061035]) # Bottom left
                ur5.move_in_routine(joints)

                #ur5.scoot_left()
                #ur5.plan_movement('wrist_3_joint',90)
                #ur5.plan_movement('wrist_2_joint',45)
                #ur5.go()
            except rospy.ROSInterruptException:
                pass



def old():  
    tf_listener = tf.TransformListener()
    tf_broadcaster= tf.TransformBroadcaster()
    rospy.sleep(2)
    running = True
    while not rospy.is_shutdown():
        for i in range(0,5):     
            while True:
                try:
                    (trans_camera,rot_camera) = tf_listener.lookupTransform('/t0', '/camera_link',rospy.Time(0))
                except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                    continue
            
                try:
                    (trans_robot,rot_robot) = tf_listener.lookupTransform('/base_link','/tool0_controller', rospy.Time(0))
                except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    continue

                trans_cam_array.append(trans_camera)
                trans_rob_array.append(trans_robot)
                rot_cam_array.append(rot_camera)
                rot_rob_array.append(rot_robot)
                print('good')

                rospy.sleep(1)
                break
        
        print('Initalisation data gathered now processing')        
        rospy.sleep(2)
        
        print('Calculating initial guess for')

        # create homogeneous rotation and translation matrices for inital point
        trans_cam = tf.transformations.translation_matrix(trans_cam_array[0])
        rot_cam   = tf.transformations.quaternion_matrix(rot_cam_array[0])
        mat_cam = tf.transformations.concatenate_matrices(trans_cam,rot_cam)


        trans_rob = tf.transformations.translation_matrix(trans_rob_array[0])
        rot_rob   = tf.transformations.quaternion_matrix(rot_rob_array[0])
        mat_rob = tf.transformations.concatenate_matrices(trans_rob,rot_rob)

        # calculate transform from base_link to camera_link for inital reading
        mat_rob2cam = tf.transformations.concatenate_matrices(mat_rob,mat_cam)

        mat3_cam2rob = tf.transformations.inverse_matrix(mat_rob2cam)

        #decompose into euler and translation
        _, _, angles, translate, _ = tf.transformations.decompose_matrix(mat3_cam2rob)

        #make single vector
        t_cam2rob =  np.concatenate((translate, angles), axis=0)

        #transform_average()

        topt_cam2rob = scipy.optimize.fmin(transform_opt, t_cam2rob)
        mat3_cam2rob = tf.transformations.inverse_matrix(mat_rob2cam)

        trans_opt = tuple(topt_cam2rob[0:3])
        angles_opt = tf.transformations.quaternion_from_euler(topt_cam2rob[3], topt_cam2rob[4], topt_cam2rob[5], axes='sxyz')
        #type(angles_opt)
        #print(type(t3opt))
        #print(t3opt)
        tf_broadcaster.sendTransform(trans_opt,angles_opt,rospy.Time.now(),'/base_link','/camera_link')
